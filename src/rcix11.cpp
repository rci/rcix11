#include <iostream>
#include "rcix11.h"

Rci::RciX11::RciX11(): _display(NULL)
{
  _display = XOpenDisplay(0);
  
  _window = XDefaultRootWindow(_display);
  XSelectInput(_display, _window, StructureNotifyMask|KeymapStateMask);
}

Rci::RciX11::~RciX11()
{
  XCloseDisplay(_display);
  _display = NULL;
}

Window Rci::RciX11::selectWindow()
{
  Window focusWindow;
  int revert;
  XGetInputFocus(_display, &focusWindow, &revert);
  return focusWindow;
}

void Rci::RciX11::checkUpdateKeyboard()
{
  /* tell the display server what kind of events we would like to see */
  XEvent event;
  int nEvent = XPending(_display);
  for(int i = 0; i < nEvent; ++i)
  {
    XNextEvent(_display, &event);
    switch(event.type)
    {
      case KeymapNotify:
        std::cout << "XMappingEvent Received" << std::endl;
        XRefreshKeyboardMapping(&(event.xmapping));
        break;
      case FocusOut:
        _window = selectWindow();
        XSelectInput(_display, _window, StructureNotifyMask|KeymapStateMask);
        break;
      default:
        break;
    }
  }
}

void Rci::RciX11::sendKeyEvent(KeySym keySym)
{
  checkUpdateKeyboard();
  Window root = XDefaultRootWindow(_display);
  Window win = selectWindow();
  
  XKeyEvent event;

  event.display     = _display;
  event.window      = win;
  event.root        = root;
  event.subwindow   = None;
  event.time        = CurrentTime;
  event.x           = 1;
  event.y           = 1;
  event.x_root      = 1;
  event.y_root      = 1;
  event.same_screen = True;
  event.keycode     = XKeysymToKeycode(_display, keySym);
  event.state       = 0;
  event.type        = KeyPress;

  XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
  
  event.type        = KeyRelease;
  
  XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
  XFlush(_display);
}

void Rci::RciX11::sendKeyEvent(const std::string& keyName)
{
  KeySym keySym = XStringToKeysym(keyName.c_str());
  sendKeyEvent(keySym);
}

void Rci::RciX11::sendTestFakeKeyEvent(KeySym keySym)
{
  Window win = selectWindow();
	XSelectInput(_display, win, StructureNotifyMask|KeymapStateMask);
  checkUpdateKeyboard();
  KeyCode keyCode = XKeysymToKeycode(_display, keySym);
  std::cout << "keySym: " << keySym << " --> keyCode: " << keyCode << std::endl;
  XTestFakeKeyEvent(_display, keyCode, True, CurrentTime);
  XTestFakeKeyEvent(_display, keyCode, False, CurrentTime);
  XFlush(_display);
}

void Rci::RciX11::sendTestFakeKeyEvent(const std::string& keyName)
{
  KeySym keySym = XStringToKeysym(keyName.c_str());
  std::cout << "keyName: " << keyName << " --> keySym: " << keySym << std::endl;
  sendTestFakeKeyEvent(keySym);
}

