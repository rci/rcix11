#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>
#include <string>

namespace Rci
{

class RciX11
{
private:
  Display* _display;
  Window _window;

protected:
  Window selectWindow();
  void checkUpdateKeyboard();
public:

  RciX11();
  virtual ~RciX11();
  
  void sendKeyEvent(KeySym);
  void sendKeyEvent(const std::string&);
  void sendTestFakeKeyEvent(KeySym);
  void sendTestFakeKeyEvent(const std::string&);
};
}
